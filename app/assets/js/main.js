(function(){

	'use strict';

	var phones = [
		// iphone 6
		{
		    width: 375,
		    height: 667 
		},

	    // samsung
	  	{
		    width: 400,
		    height: 640 
	  	},

	    // microsoft
		{
			width: 320,
		    height: 480  
		},

	    // htc
		{
		    width: 360,
		    height: 640 
		},

	    // ipad mini
	  	{
		    width: 768,
		    height: 1024 
		},
	];


	/*Only needed for the controls*/
	var $phone 	= $("#phone_1"),
		iframe 	= document.getElementById("frame_1"),
		phoneSelected = 0,
		orientationSelected = 1;


	/*View*/
	function updateView(e) {
		var $el = $(e.currentTarget);
		var view = $el.val();

		if($el.hasClass('active'))
			return;
		else{

			$el.siblings()
				.removeClass('active');
			$el.addClass('active');

		}

	  	if (view) {
	    	$phone
	    		.removeClass(function(i, c) {
					return c.match(/view_\d+/g).join(" ");
				})
	    		.addClass("view_" + view);
	  	}
	}

	/*Phones*/
	function updatePhone(evt) {

		var $el = $(evt.currentTarget);

		if($el.hasClass('active'))
			return;
		else{
			$el.siblings()
				.removeClass('active');
			$el.addClass('active');
		}

		phoneSelected = $el.val()-1;
		var resolution = getResolution(phoneSelected);

		$phone.css(resolution)
	}

	/*Controls*/
	function updateIframe() {

	  	// preload iphone width and height
		$phone.css(phones[phoneSelected])

	  	/*Idea by /u/aerosole*/
	  	document.getElementById("wrapper").style.perspective = (
	    	document.getElementById("iframePerspective").checked ? "1300px" : "none"
	  	);

	}

	/*Orientation*/
	function updateOrientation(evt) {
		var $el = $(evt.currentTarget);
		
		orientationSelected = Number($el.val());

		if($el.hasClass('active'))
			return;
		else{
			$el.siblings()
				.removeClass('active');
			$el.addClass('active');
		}

		var resolution = getResolution();

		$phone
	    	.removeClass('pt lc')
	    	.addClass(orientationSelected == 1 ? 'pt' : 'lc');

		$phone.css(resolution);

	}

	function getResolution(ps, or) {
		or = or || orientationSelected;
		ps = ps || phoneSelected;


		var resolution = {};

		if(or === 1){

			resolution = phones[ps];

		}else if(or === 2){

			resolution.width = phones[ps].height;
			resolution.height = phones[ps].width;
		}

		return resolution;
	}


	updateIframe();

	/*Events*/
	$('#controls input').on('change', updateIframe);

	$('#views button').on('click', updateView);

	$('#phones button').on('click', updatePhone);

	$('#orientation button').on('click', updateOrientation);


	iframe = document.getElementById('frame_1');

	if (iframe.attachEvent){
	    iframe.attachEvent("onload", function(){
			afterLoading();
	    });
	} else {
	    iframe.onload = function(){
	        afterLoading();
	    };
	}

	function afterLoading(){

	    setTimeout(function() {
	       	$phone
	    		.removeClass()
	    		.addClass("phone view_2 pt");
	   //      setTimeout(function() {
	   //          // do second thing
				// $phone
		  //   		.removeClass()
		  //   		.addClass("phone view_1 rotate");
	   //      }, 1000);
	    }, 1000);

	}

}());